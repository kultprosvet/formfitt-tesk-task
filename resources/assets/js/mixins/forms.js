import EditorToolbar from '../components/EditorToolbar'

export default {
    data() {
        return {
            pages: [],
            activePage: 0,
            showFields: true,
            pagesReady: false
        }
    },
    computed: {
        offsetLeft(){
            return  this.$refs['page'+this.activePage][0].offsetLeft
        },
        offsetTop(){
            return  this.$refs['page'+this.activePage][0].offsetTop
        }
    },
    mounted() {
        this.$root.showLoader = true;
        this.apiGet('get-pages').then(response => {
            this.pages = response.data;
            this.$root.showLoader = false;
            this.imagesAreLoaded();
            if(this.pages.length == 0) {
                this.$root.error = 'Please upload the PDF document first'
                this.$router.replace('/')
            }
        });
        window.addEventListener('resize', this.relocateFields)
    },
    methods: {
        setActivePage(id){
            this.activePage = id;
        },
        imagesAreLoaded(attempts=0){
            setTimeout(() =>{
                if(this.$refs.docImage && this.$refs.docImage.length === this.getObjectSize(this.pages)) {
                    this.relocateFields()
                    return true;
                }else if(attempts < 20){
                    this.imagesAreLoaded(++attempts)
                }
            }, 200)
        },
        relocateFields() {
            if(!this.$refs.docImage) return;
            for(const pageIndex in this.pages){
                for(const field of this.pages[pageIndex].fields){
                    if(!this.$refs.docImage[pageIndex-1]) continue;
                    const widthCorrelation = this.$refs.docImage[pageIndex-1].width / this.$refs.docImage[pageIndex-1].naturalWidth;
                    const heightCorrelation = this.$refs.docImage[pageIndex-1].height / this.$refs.docImage[pageIndex-1].naturalHeight;
                    field.relativeX = parseInt(field.x * widthCorrelation);
                    field.relativeY = parseInt(field.y * heightCorrelation);
                    field.relativeWidth = parseInt(field.width * widthCorrelation);
                    field.relativeHeight = parseInt(field.height * heightCorrelation);
                }
            }

            // Rerender fields with new values
            this.showFields = false;
            this.$nextTick(() => {
                this.showFields = true;
            })
        },

    },
    components: {
        EditorToolbar
    }
}
