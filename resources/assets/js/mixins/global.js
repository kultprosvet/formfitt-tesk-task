export default {
    data() {
        return {
            message: '',
            error: '',
            showLoader: false
        }
    },
    watch: {
        '$root.message': function () {
            if (this.$root.message) {
                $('#MessageModal').modal('show')
            }
        },
        '$root.error': function () {
            if (this.$root.error) {
                $('#MessageModal').modal('show')
            }
        }
    },
    methods: {
        closeMessageModal() {
            this.$root.message = ''
            this.$root.error = ''
            $('#MessageModal').modal('hide')
        },
        getScrollTop() {
            if (typeof pageYOffset != 'undefined') {
                //most browsers except IE before #9
                return pageYOffset;
            }
            else {
                var B = document.body; //IE 'quirks'
                var D = document.documentElement; //IE with doctype
                D = (D.clientHeight) ? D : B;
                return D.scrollTop;
            }
        },
        getObjectSize(obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        }
    }
}
