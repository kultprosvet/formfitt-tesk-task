import axios from './axios'
import { baseURL } from '../../mixins/api/axios'

export default {
    data(){
        return {
            baseURL
        }
    },
    methods: {
        apiUrl(url){
          return  this.baseURL + url;
        },
        getApiConfig() {

            let token = this.getApiToken()
            let config = {}

            if (token) {
                config.headers = {
                    'Authorization': 'Bearer ' + token
                }
            }
            return config
        },

        getApiConfigFile() {
            let token = this.getApiToken()
            let config = {}

            if (token) {
                config.headers = {
                    // 'Authorization': 'Bearer ' + token,
                    'Content-Type': 'multipart/form-data'
                }
            }
            return config
        },

        apiUpdate(url, data) {
            // let formData = new FormData()
            // for (let fld in data) {
            //   formData.append(fld, data[fld])
            // }
            return axios.put(this.apiUrl(url), data, this.getApiConfig())
                .then(res => {
                    return res
                })
                .catch(function (error) {
                });
        },

        apiPost(url, data) {

            let formData = new FormData()
            for (let fld in data) {
                formData.append(fld, data[fld])
            }
            return axios.post(this.apiUrl(url), data)
        },

        apiPost_(url, data, config) {
            let formData = new FormData()
            for (let fld in data) {
                formData.append(fld, data[fld])
            }
            let mergedConfig = {...this.getApiConfig(), ...config};
            return axios.post(this.apiUrl(url), formData, mergedConfig)
        },

        apiPost_File(url, data) {
            return axios.post(this.apiUrl(url), data, this.getApiConfigFile())
        },


        apiGet(url, data) {
            return axios.get(this.apiUrl(url), this.getApiConfig())
        },

        apiGet_(url, data) {
            return axios.get(this.apiUrl(url), {
                headers: {
                    'Authorization': 'Basic',
                    'Accept': 'application/json',
                    'X-CSRF-Token': 'Required'
                },
                params: data
            })
        },

        apiDelete(url, data) {
            url = this.apiUrl(url) + data
            return axios.delete(url, this.getApiConfig())
        },

        getTokenAfterRefresh() {
            let token = this.user.api_token
            let config = {}

            if (token) {
                config.headers = {
                    'Authorization': 'Bearer ' + token
                }
            }
            return config
        },

        getApiToken() {

            return true; // skip the auth for test

            let token = this.user.api_token
            if (!token) {
                this.$store.commit('setUserData', {})
                this.$router.push('/')
            }
            return token
        },

    },
}
