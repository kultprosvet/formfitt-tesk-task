import axios from 'axios'

const instance = axios.create({})

export default instance

export const baseURL =  '/api/v1/'