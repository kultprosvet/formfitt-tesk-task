
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// Vue.config.devtools = true;

import router from './router'
import apiMixIn from './mixins/api/api'
import globalMixIn from './mixins/global'
import VueDraggableResizable from 'vue-draggable-resizable'


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('main-toolbar', require('./components/MainToolbar.vue'))
Vue.component('message-modal', require('./components/modal/MessageModal.vue'))
Vue.component('loader', require('./components/Loader'))
Vue.component('vue-draggable-resizable', VueDraggableResizable)

Vue.mixin(apiMixIn)
Vue.mixin(globalMixIn)

const app = new Vue({
    el: '#app',
    router
});
