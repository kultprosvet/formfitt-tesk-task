import MainToolbar from '../components/MainToolbar'
import EditForm from '../components/EditForm'
import FillForm from '../components/FillForm'


export const routes = [
	{ path: '/', component:  MainToolbar },
    { path: '/edit-form', component: EditForm },
    { path: '/fill-form', component: FillForm }
]