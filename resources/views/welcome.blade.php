<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="theme-color" content="#000000">
        <title>FormFitt</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        @php
            $v = bin2hex(openssl_random_pseudo_bytes(10));
        @endphp
        <link href="/css/app.css<?= '?v='.$v;?>" rel="stylesheet" type="text/css">
    </head>
    <body>

        <div id="app">
            <router-view></router-view>
            <message-modal></message-modal>
            <loader v-if="showLoader"></loader>
        </div>
        <script src="/js/app.js<?= '?v='.$v;?>"></script>
    </body>
</html>
