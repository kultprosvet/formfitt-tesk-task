# Installing on on the brand new server:

## Requirements:
* PHP 7.1+
* Composer
* Apache (Nginx) 
* Node.js
* MySQL 5.7+


## 1. Clone the git repository:

```
git clone https://superkultprosvet@bitbucket.org/kultprosvet/formfitt-tesk-task.git [directory_name]
```
or if you have an SSH key access

```
git clone git@bitbucket.org:kultprosvet/formfitt-tesk-task.git [directory_name]
```

and switch to the project directpory (`[new_brand_directory]`):
```
cd [new_brand_directory]
```


## 2. Install all the composer dependecies:

### Install the composer (if it not exists on the server)

[Composer installation manual](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)

```
composer install
```

## 3. Create the symbolic link for the files Storage

```
php artisan storage:link
```

## 4. Create the MySQL database for the project


## 5. Create the .env file to set the project configuration


## 6. Run database migrations

```php
php artisan migrate
```

### 7. Install all the NPM dependencies

Install the node.js and npm if they are not installed:

```
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install npm
```

Install NPM dependencies:

```
npm install
```

### 8. Build the project:

```
npm run prod
```

### 9. Point the webroot for webserver to

`[directory_name]/public`


### 10. Make sure that your webroot directory and files have the necessary permissions for the web server

e.g. for Ubuntu permissions can be set by:

```
sudo chown -R  www-data:www-data /var/www/formfit/  
```

### 11. Make sure `imagemagick` is installed on your server. If needed it can be installed the next way:

```
sudo apt update
sudo apt install imagemagick
```
