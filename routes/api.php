<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1'], function () {
    Route::post('pdf-import', 'PageController@pdfImport');
    Route::get('get-pages', 'PageController@index');
    Route::get('print', 'PageController@printForm');
    Route::post('save-fields', 'FieldController@store');
    Route::post('save-values', 'ValueController@store');
});
