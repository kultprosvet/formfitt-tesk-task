<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Value extends Model
{
    protected $fillable = ['field_id', 'value'];


    public function field()
    {
        return $this->belongsTo('App\Field', 'field_id');
    }
}
