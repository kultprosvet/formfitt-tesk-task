<?php

namespace App\Http\Controllers;

use App\Page;
use App\Field;
use App\Value;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use setasign\Fpdi;


class PageController extends Controller
{
    use ApiResponser;

    public function index(Request $request)
    {
        return response()->json(Page::with('fields')->get()->keyBy('id'));
    }


    public function pdfImport(Request $request) {
        $report = '';
        $pagesQty = 0;

        // Check the uploaded file extension
        if($request->pdf->extension() != 'pdf') {
            $report .= "<p class='error'>File must be in PDF format</p>";
            return $this->showMessage($report, 200);
        }

        // Save the PDF file
        self::emptyDirectory('public/pdf');
        $pdf_file_name = $request->pdf->getClientOriginalName();
        $pdf_path = $request->pdf->storeAs('public/pdf', $pdf_file_name);
        $path = storage_path('app') .'/'. $pdf_path;


        // Convert PDF to JPG
        if (!file_exists(storage_path('app') .'/public/jpg')) {
            mkdir(storage_path('app') .'/public/jpg', 0755, true);
        } else {
            self::emptyDirectory('public/jpg');
        }

        $convert    = '"' . $path .'"' . ' "'. storage_path('app') . '/public/jpg/image.jpg"';
        shell_exec('export PATH=$PATH:/usr/bin&& convert -density 150 ' . $convert);


        // Delete the previous document from the DB
        self::clearDB();


        // Store pages to the DB
        $pagesQty = self::countPages($path);
        if($pagesQty > 1) {
            for ($i = 0; $i < self::countPages($path); $i++) {
                Page::create(["path"=>"storage/jpg/image-$i.jpg", "original_path"=>$pdf_path]);
            }
        }else {
            Page::create(["path"=>"storage/jpg/image.jpg", "original_path"=>$pdf_path]);
        }

        $pages = $pagesQty == 1 ? 'page' : 'pages';
        $report .= "<p class='success'>PDF file has been uploaded</p><p>$pagesQty $pages uploaded successfully</p>";
        return $this->showMessage($report, 200);
    }


    public function printForm() {

        $original_pdf_path = storage_path('app') .'/'. Page::first()->original_path;
        $pagesQty = self::countPages($original_pdf_path);
        $pagesData = Page::with('fields')->get()->keyBy('id');

        $pdf = new Fpdi\TcpdfFpdi();
        $pdf->setSourceFile($original_pdf_path);
        $pdf->SetFont('dejavusans', '', 10, '', true);
//        $pdf->SetTextColor(0, 0, 0);

        for($i=1; $i<=$pagesQty; $i++) {
            $pageData = $pagesData->slice($i - 1, 1)->first();
            $tplId = $pdf->importPage($i);
            $pdfSize = $pdf->getTemplateSize($tplId);
            $imagePath = storage_path('app') .'/'. str_replace('storage', 'public', $pageData->path);
            $imageSize = getimagesize($imagePath);
            $pdfFormat = $pdfSize['height'] > 148 && $pdfSize['height'] < 209 ? 'A5' : 'A4';
            $pdf->AddPage($pdfSize['orientation'], $pdfFormat);
            $pdf->useTemplate($tplId, 0, 0, $pdfSize['width'], $pdfSize['height']);

            foreach ($pageData->fields as $field) {
                $pdf->SetXY($field->x*$pdfSize['width']/$imageSize[0]-3, $field->y*$pdfSize['height']/$imageSize[1]);
                $pdf->Write(0, $field->value->value);
            }
        }

        $pdf->Output();
    }


    public static function countPages($pdfname)
    {
        $pdftext = file_get_contents($pdfname);
        $num = preg_match_all("/\/Page\W/", $pdftext, $dummy);
        return $num;
    }


    public static function emptyDirectory($dir)
    {
        $path = storage_path('app') .'/'. $dir .'/*';
        $files = glob($path);
        foreach($files as $file){
            if(is_file($file))
                unlink($file);
        }
    }


    public static function clearDB(){
        DB::statement("SET foreign_key_checks=0");
        Value::truncate();
        Field::truncate();
        Page::truncate();
        DB::statement("SET foreign_key_checks=1");
    }
}
