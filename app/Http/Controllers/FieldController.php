<?php

namespace App\Http\Controllers;

use App\Field;
use App\Traits\ApiResponser;
use App\Value;
use Illuminate\Http\Request;

class FieldController extends Controller
{
    use ApiResponser;

    public function store(Request $request)
    {
        $requestData = $request->all();
        foreach ($requestData as $page) {
            foreach($page['fields'] as $field) {

                if(!empty($field['delete']) && !empty($field['id'])) {
                    Value::where('field_id', $field['id'])->delete();
                    Field::destroy($field['id']);
                    continue;
                }
                $savedField = Field::updateOrCreate(['id' => $field['id'], 'page_id' => $field['page_id']], $field);
                Value::updateOrCreate(['field_id' => $savedField->id],
                    [   'value' => !empty($field['value']) ? $field['value']['value'] : '',
                        'field_id' => $savedField->id
                    ]);
            }
        }

        $report = "<p class='success'>Document`s fields are saved</p>";
        return $this->showMessage($report, 200);
    }
}
