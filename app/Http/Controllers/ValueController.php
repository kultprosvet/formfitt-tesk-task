<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use App\Value;
use Illuminate\Http\Request;

class ValueController extends Controller
{
    use ApiResponser;

    public function store(Request $request)
    {
        $requestData = $request->all();
        foreach ($requestData as $page) {
            foreach($page['fields'] as $field) {
                Value::updateOrCreate(['field_id' => $field['id']],
                    ['value' => !empty($field['value']) ? $field['value']['value'] : '',
                        'field_id' => $field['id']
                    ]);
            }
        }

        $report = "<p class='success'>Document`s fields values are saved</p>";
        return $this->showMessage($report, 200);
    }
}
