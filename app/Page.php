<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['path', 'original_path'];


    public function fields()
    {
        return $this->hasMany('App\Field')->with('value');
    }

}
