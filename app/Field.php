<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{

    protected $fillable = ['page_id', 'x', 'y', 'width', 'height'];


    public function page()
    {
        return $this->belongsTo('App\Page', 'page_id');
    }


    public function value()
    {
        return $this->hasOne('App\Value');
    }
}
